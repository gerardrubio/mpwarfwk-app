<?php

require_once __DIR__ . '/..' . '/vendor' . '/autoload.php';

use Mpwarfwk\Bootstrap;
use Mpwarfwk\Request\HttpRequest;
use Mpwarfwk\Session\Session;

$kernel = new Bootstrap(__DIR__ . '/..' . '/src' . '/app_dev.yml', \Mpwarfwk\Settings\Environment::DEVELOPMENT);
$kernel->execute(new HttpRequest(new Session));
