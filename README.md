MPWAR Framework
===============
Gerard Rubio [tm13939@salleurl.edu / gerard.rubio@gmail.com]

Instrucciones de uso:
1. Ejecutar composer update
2. Añadir entradas en apache
3. Añadir entradas en hosts
4. Apuntar el navegador al dominio especificado ya está :)

Para la base de datos, se ha implementado con SQLite, con lo cual deberías tener instalada la dependencia correspondiente.

Disponemos de dos entornos:
- __Producción__: gestionado por ```/public/index.php``` y ```/src/app.yml```
- __Desarrollo__: gestinado por ```/public/index_dev.php``` y ```/src/app_dev.yml```

Todos los ficheros de configuración están soportados en los siguientes formatos:
- Yaml
- Json
- PHP

Para usarlos, solo hace falta especificar la ruta del fichero y la aplicación se encarga de detectar el tipo y cargarlo como corresponda.

En la definición de la aplicación (/src/app[_dev].yml) podemos definir casi todos los aspectos de nuestro proyecto.
Disponemos de dos accesos directos para las rutas más comunes:

- __\#app\#__: Define la ruta a /src
- __\#public\#__: Define la ruta a /public
- __@__: Define un servicio o dependencia definida en el fichero de configuración

Campos usados:
```
controllers: '#app#/Controllers'  # Ruta de los controladores
routes: '#app#/Routes/routes.yml' # Fichero de configuración de rutas
dependencies: # Dependencias a gestionar por el contenedor de dependencias
  'templating': # Nombre de la dependencia
    class: 'Mpwarfwk\Template\TwigTemplate' # Clase referenciada
    arguments: # Parámetros del constructor, pueden ser rutas, otras dependencias o valores
      - '#app#/Views'
      - false # Ruta de la cache o false si queremos desactivarla
  'smarty':
    class: 'Mpwarfwk\Template\SmartyTemplate'
    arguments:
      - '#app#/Views'
      - false
  'db':
    class: 'Mpwarfwk\Database\PDO'
    arguments:
      - '@sqlite_connection'
    persistent: true
  'sqlite_connection':
    class: 'Mpwarfwk\Database\SQLiteConfiguration'
    arguments:
      - '#app#/mpwarfwk.sqlite'
  'i18n':
    class: 'Mpwarfwk\I18n\Translation'
    arguments:
      - '@i18n_repository'
      - '#app#/I18n'
    persistent: true
  'i18n_repository':
    class: 'Mpwarfwk\I18n\TranslationRepository'
```

