<?php
/**
 * Created by PhpStorm.
 * User: grubio
 * Date: 3/11/15
 * Time: 7:35 PM
 */

namespace Controllers;

use Mpwarfwk\Controller\Base;
use Mpwarfwk\Database\Configuration;
use Mpwarfwk\Request\IRequest;
use Mpwarfwk\Response\HttpResponse;
use Mpwarfwk\Response\HttpStatusCodes;
use Mpwarfwk\Response\IResponse;
use Mpwarfwk\Response\JsonResponse;

class Home extends Base
{
    /**
     * Home page
     * @param IRequest $request
     * @return IResponse
     */
    public function indexAction(IRequest $request)
    {
        $template = $this->dependency('templating');
        $template->prepare('/index.twig');

        $response = new HttpResponse($template);
        return $response;
    }

    /**
     * Json page
     * @param IRequest $request
     * @return IResponse
     */
    public function jsonAction(IRequest $request)
    {
        $name = $request->get('name', 'Foo');
        $data = ['name' => $name];
        return new JsonResponse($data);
    }

    /**
     * Smarty page
     * @param IRequest $request
     * @return IResponse
     */
    public function smartyAction(IRequest $request)
    {
        $template = $this->dependency('smarty');
        $template->prepare('template.tpl', [
            'foo' => $request->get('name', 'Foo')
        ]);
        $response = new HttpResponse($template);
        return $response;
    }

    /**
     * PDO page
     * @param IRequest $request
     * @return IResponse
     */
    public function pdoAction(IRequest $request)
    {
        $db = $this->dependency('db');
        $dbConfig = new Configuration;
        $dbConfig->driver = Configuration::MYSQL;
        $dbConfig->name = 'swoe';
        $dbConfig->host = 'localhost';
        $dbConfig->port = 3306;
        $dbConfig->username = 'root';
        $dbConfig->password = 'strongpassword';

        $db->connect($dbConfig);
        $results = $db->query("SELECT * FROM LogicalDeviceEntry WHERE a_media = :media LIMIT 1, 5", [
            ':media' => 1
        ]);

        $template = $this->dependency('templating');
        $template->prepare('/pdo.twig', ['entries' => $results]);
        $response = new HttpResponse($template);
        return $response;
    }

    /**
     * SQLite page
     * @param IRequest $request
     * @return IResponse
     */
    public function sqliteAction(IRequest $request)
    {
        $db = $this->dependency('db');
        $dbConfig = new Configuration;
        $dbConfig->driver = Configuration::SQLITE;
        $dbConfig->file = __DIR__ . '/..' . '/mpwarfwk.sqlite';

        $db->connect($dbConfig);
        $results = $db->query("SELECT * FROM posts ORDER BY posted_on DESC");

        $template = $this->dependency('templating');
        $template->prepare('/sqlite.twig', ['posts' => $results]);
        $response = new HttpResponse($template);
        return $response;
    }

    /**
     * Doctrine page
     * @param IRequest $request
     * @return IResponse
     */
    public function doctrineAction(IRequest $request)
    {
        $response = new HttpResponse("");
        return $response;
    }

    public function translateAction(IRequest $request)
    {
        $translator = $this->dependency('i18n');
        try
        {
            $translator->setLanguage($request->get('lang', 'es-ES'));
        } catch (\Exception $e)
        {
        }

        $data = [
            ['key' => 'Hola', 'value' => $translator->get('Hola')->translation],
            ['key' => 'Adéu', 'value' => $translator->get('Adéu')->translation],
            ['key' => 'Menú', 'value' => $translator->get('Menú')->translation],
            ['key' => 'Configuració', 'value' => $translator->get('Configuració')->translation],
        ];

        $template = $this->dependency('templating');
        $template->prepare('/i18n.twig', [
            'title' => $translator->get('Traduccions')->translation,
            'change_language' => $translator->get('Canviar idioma')->translation,
            'language' => $translator->language(),
            'translations' => $data
        ]);

        $response = new HttpResponse($template);
        return $response;
    }

    public function notFoundAction(IRequest $request)
    {
        $template = $this->dependency('templating');
        $template->prepare('/404.twig');

        $response = new HttpResponse($template, HttpStatusCodes::NOT_FOUND);
        return $response;
    }
}
