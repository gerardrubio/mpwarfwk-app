<?php
/**
 * Created by PhpStorm.
 * User: grubio
 * Date: 3/11/15
 * Time: 7:35 PM
 */

namespace Controllers;

use Mpwarfwk\Controller\Base;
use Mpwarfwk\DependencyInjection\Container;
use Mpwarfwk\Request\HttpRequest;
use Mpwarfwk\Request\IRequest;
use Mpwarfwk\Response\HttpResponse;
use Mpwarfwk\Response\IResponse;
use Mpwarfwk\Response\JsonResponse;
use Mpwarfwk\Response\RedirectResponse;

class Blog extends Base
{
    /**
     * Home page
     * @param IRequest $request
     * @return IResponse
     */
    public function homeAction(IRequest $request)
    {
        $template = Container::getInstance()->get('templating');
        $db = Container::getInstance()->get('db');
        $posts = $db->query("SELECT p.id, p.title, p.contents, p.posted_on, p.author AS `author_id`, u.username AS `author` FROM posts AS p LEFT JOIN users AS u ON p.author = u.id ORDER BY p.posted_on DESC");
        $template->prepare('/Blog/home.twig', ['posts' => $posts]);

        $response = new HttpResponse($template);
        return $response;
    }

    public function authorAction(IRequest $request)
    {
        $template = Container::getInstance()->get('templating');
        $db = Container::getInstance()->get('db');

        $queryParams = [':id' => $request->get('author')];
        $authors = $db->query("SELECT * FROM users WHERE id = :id LIMIT 1", $queryParams);

        if (empty($authors))
        {
            return $this->pageNotFoundAction($request);
        }

        $query = "SELECT p.id, p.title, p.contents, p.posted_on, p.author AS `author_id`, u.username AS `author` FROM posts AS p LEFT JOIN users AS u ON p.author = u.id WHERE u.id = :id ORDER BY p.posted_on DESC";
        $posts = $db->query($query, $queryParams);

        $template->prepare('/Blog/author.twig', ['posts' => $posts, 'author' => $authors[0]['username']]);

        $response = new HttpResponse($template);
        return $response;
    }

    public function postAction(IRequest $request)
    {
        $template = Container::getInstance()->get('templating');
        $db = Container::getInstance()->get('db');
        $query = "SELECT p.id, p.title, p.contents, p.posted_on, p.author AS `author_id`, u.username AS `author` FROM posts AS p LEFT JOIN users AS u ON p.author = u.id WHERE p.id = :id ORDER BY p.posted_on DESC";
        $queryParams = [':id' => $request->get('post')];
        $posts = $db->query($query, $queryParams);

        if (empty($posts))
        {
            $this->pageNotFoundAction($request);
            return;
        }

        $template->prepare('/Blog/post.twig', ['post' => $posts[0]]);

        $response = new HttpResponse($template);
        return $response;
    }

    public function searchAction(IRequest $request)
    {
        $template = Container::getInstance()->get('templating');
        $db = Container::getInstance()->get('db');

        $searchTerm = $request->get('term', '');
        if (!empty($searchTerm)) {
            $query = "SELECT p.id, p.title, p.contents, p.posted_on, p.author AS `author_id`, u.username AS `author` FROM posts AS p LEFT JOIN users AS u ON p.author = u.id WHERE p.title LIKE :term OR p.contents LIKE :term ORDER BY p.posted_on DESC";
            $queryParams = [':term' => "%{$searchTerm}%"];
            $posts = $db->query($query, $queryParams);
        } else {
            $posts = [];
        }

        $template->prepare('/Blog/search.twig', ['posts' => $posts, 'term' => $searchTerm]);

        $response = new HttpResponse($template);
        return $response;
    }

}
