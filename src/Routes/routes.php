<?php

return [
    "home" => [
        "route" => "/",
        "action" => "Controllers\\Home::index"
    ],
    "twig" => [
        "route" => "/twig",
        "action" => "Controllers\\Home::twig"
    ],
    "smarty" => [
        "route" => "/smarty",
        "action" => "Controllers\\Home::smarty"
    ],
    "template" => [
        "route" => "/template",
        "action" => "Controllers\\Home::template"
    ],
    "test" => [
        "route" => "/test",
        "action" => "Controllers\\Home::test"
    ],
    "json" => [
        "route" => "/json",
        "action" => "Controllers\\Home::json"
    ],
    "pdo" => [
        "route" => "/pdo",
        "action" => "Controllers\\Home::pdo"
    ],
    "sqlite" => [
        "route" => "/sqlite",
        "action" => "Controllers\\Home::sqlite"
    ],
    "doctrine" => [
        "route" => "/doctrine",
        "action" => "Controllers\\Home::doctrine"
    ],
    "admin_login" => [
        "route" => "/admin",
        "action" => "Controllers\\Admin\\Home::login"
    ],
    "admin_signup" => [
        "route" => "/admin/sign-up",
        "action" => "Controllers\\Admin\\Home::signUp"
    ],
    "i18n" => [
        "route" => "/i18n",
        "action" => "Controllers\\Home::translate"
    ]
];
